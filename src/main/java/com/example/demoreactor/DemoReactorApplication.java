package com.example.demoreactor;

import com.example.demoreactor.operador.combinacion.Combinacion;
import com.example.demoreactor.operador.condicion.Condicional;
import com.example.demoreactor.operador.error.ErrorOp;
import com.example.demoreactor.operador.filtrado.Filtrado;
import com.example.demoreactor.model.Persona;
import com.example.demoreactor.operador.matematico.Matematico;
import io.reactivex.Observable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoReactorApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(DemoReactorApplication.class);

	public void reactor(){
		Mono.just(new Persona(1,"Me",25))
				.doOnNext(p -> {
					//Logica Adicional
					log.info("[Reactor] Persona: " + p);
				})
				.subscribe(p -> log.info("[Reactor] Persona: " + p));
	}

	public void rxjava2(){
		Observable.just(new Persona(1,"Me",25))
				.doOnNext(p -> log.info("[RxJava2] Persona: " + p))
				.subscribe(p -> log.info("[RxJava2] Persona: " + p));
	}

	public void mono(){
		Mono.just(new Persona(1,"Me",25))
				.subscribe(p-> log.info(p.toString()));
	}

	public void flux(){
		List<Persona> personas = new ArrayList<>();
		personas.add(new Persona(1, "Mito", 27));
		personas.add(new Persona(2, "Mitoc", 28));
		personas.add(new Persona(3, "Mitoco", 29));

		Flux.fromIterable(personas).subscribe(p -> log.info(p.toString()));
	}

	public void fluxMono() {
		List<Persona> personas = new ArrayList<>();
		personas.add(new Persona(1, "Mito", 27));
		personas.add(new Persona(2, "Mitoc", 28));
		personas.add(new Persona(3, "Mitoco", 29));

		Flux<Persona> fx = Flux.fromIterable(personas);
		fx.collectList().subscribe(lista -> log.info(lista.toString()));
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoReactorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*
		reactor();
		rxjava2();
		mono();
		flux();
		fluxMono();
		*/

		/*
		Creacion app = new Creacion();
		app.range();
		app.repeat();
		*/

		/*
		Transformacion app = new Transformacion();
		app.map();
		app.flatMap();
		app.groupBy();
		*/

		/*
		Filtrado app = new Filtrado();
		app.filter();
		app.distinc();
		app.take();
		app.takeLast();
		app.skip();
		app.skipLast();
		*/

		/*
		Combinacion app = new Combinacion();
		app.merge();
		app.zip();
		app.zipWith();
		*/

		/*
		ErrorOp app = new ErrorOp();
		// app.retry();
		app.errorReturn();
		app.errorResume();
		app.errorMap();
		*/

		/*
		Condicional app = new Condicional();
		app.defaultIfEmpty();
		app.takeUntil();
		app.tiemout();
		*/

		Matematico app = new Matematico();
		app.average();
		app.count();
		app.min();
		app.sum();
		app.summarizing();
	}
}
