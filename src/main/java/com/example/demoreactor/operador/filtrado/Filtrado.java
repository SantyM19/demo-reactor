package com.example.demoreactor.operador.filtrado;

import com.example.demoreactor.model.Persona;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

public class Filtrado {
    private static final Logger log = LoggerFactory.getLogger(Filtrado.class);

    public void filter(){
        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas)
                .filter(p -> p.getEdad()>28)
                .subscribe(x -> log.info(x.toString()));

    }

    public void distinc(){
        // Depende del Generetae -> HasCode Equals

        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas)
                .distinct()
                .subscribe(x -> log.info(x.toString()));
    }

    public void take(){

        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        // desde inicio -> tomara 2

        Flux.fromIterable(personas)
                .take(2)
                .subscribe(x -> log.info(x.toString()));
    }

    public void takeLast(){

        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        // desde atras -> tomara 2

        Flux.fromIterable(personas)
                .takeLast(2)
                .subscribe(x -> log.info(x.toString()));
    }

    public void skip(){

        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        // se saltara la cantidad de elemtos -> number desde el inicio

        Flux.fromIterable(personas)
                .skip(1)
                .subscribe(x -> log.info(x.toString()));
    }

    public void skipLast(){

        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        // se saltara la cantidad de elemtos -> number desde atras

        Flux.fromIterable(personas)
                .skipLast(1)
                .subscribe(x -> log.info(x.toString()));
    }

}
