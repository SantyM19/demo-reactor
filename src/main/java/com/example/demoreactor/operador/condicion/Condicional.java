package com.example.demoreactor.operador.condicion;

import com.example.demoreactor.model.Persona;
import com.example.demoreactor.operador.combinacion.Combinacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Condicional {
    private static final Logger log = LoggerFactory.getLogger(Combinacion.class);

    public void defaultIfEmpty(){
        Mono.just(new Persona(1,"Damian",29))
        //Mono.empty()
        //Flux.empty()
                .defaultIfEmpty(new Persona(0,"DEFAULT",99))
                .subscribe(x -> log.info(x.toString()));
    }

    public void takeUntil(){
        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas)
                .takeUntil(p -> p.getEdad() > 27)
                .subscribe(x -> log.info(x.toString()));
    }

    public void tiemout() throws InterruptedException {
        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona(1, "Mito", 27));
        personas.add(new Persona(2, "Mitoc", 28));
        personas.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas)
                .delayElements(Duration.ofSeconds(1))
                .timeout(Duration.ofSeconds(2))
                .subscribe(x -> log.info(x.toString()));

        Thread.sleep(10000);
    }
}
