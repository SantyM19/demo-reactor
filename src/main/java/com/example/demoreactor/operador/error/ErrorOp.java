package com.example.demoreactor.operador.error;

import com.example.demoreactor.model.Persona;
import com.example.demoreactor.operador.combinacion.Combinacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

public class ErrorOp {
    private static final Logger log = LoggerFactory.getLogger(Combinacion.class);
    public void retry(){

        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas1)
                .concatWith(Flux.error(new RuntimeException("Un Error")))
                .retry(1)
                .doOnNext(x -> log.info(x.toString()))
                .subscribe();
    }

    public void errorReturn(){

        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas1)
                .concatWith(Flux.error(new RuntimeException("Un Error")))
                .onErrorReturn(new Persona(0, "xyz", 99))
                .subscribe(x -> log.info(x.toString()));
    }

    public void errorResume(){
        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas1)
                .concatWith(Flux.error(new RuntimeException("Un Error")))
                .onErrorResume(e -> Mono.just(new Persona(0, "xyz", 99)))
                .subscribe(x -> log.info(x.toString()));
    }

    public void errorMap(){
        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        Flux.fromIterable(personas1)
                .concatWith(Flux.error(new RuntimeException("Un Error")))
                .onErrorMap(e -> new InterruptedException(e.getMessage()))
                .subscribe(x -> log.info(x.toString()));
    }
}
