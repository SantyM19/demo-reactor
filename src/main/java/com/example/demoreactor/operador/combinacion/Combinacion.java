package com.example.demoreactor.operador.combinacion;

import com.example.demoreactor.model.Persona;
import com.example.demoreactor.model.Ventas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Combinacion {
    private static final Logger log = LoggerFactory.getLogger(Combinacion.class);

    public void merge(){
        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        List<Persona> personas2 = new ArrayList<>();
        personas2.add(new Persona(4, "Mitocod", 27));
        personas2.add(new Persona(5, "Mitoccode", 28));
        personas2.add(new Persona(6, "Mitocodedee", 29));

        List<Ventas> ventas =  new ArrayList<>();
        ventas.add(new Ventas(1, LocalDateTime.now()));

        Flux<Persona> fx1 = Flux.fromIterable(personas1);
        Flux<Persona> fx2 = Flux.fromIterable(personas2);
        Flux<Ventas> fx3 = Flux.fromIterable(ventas);

        Flux.merge(fx1,fx2, fx3).subscribe(p -> log.info(p.toString()));
    }

    public void zip(){
        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        List<Persona> personas2 = new ArrayList<>();
        personas2.add(new Persona(4, "Mitocod", 27));
        personas2.add(new Persona(5, "Mitoccode", 28));
        personas2.add(new Persona(6, "Mitocodedee", 29));

        List<Ventas> ventas =  new ArrayList<>();
        ventas.add(new Ventas(1, LocalDateTime.now()));

        Flux<Persona> fx1 = Flux.fromIterable(personas1);
        Flux<Persona> fx2 = Flux.fromIterable(personas2);
        Flux<Ventas> fx3 = Flux.fromIterable(ventas);

        Flux.zip(fx1, fx2, (p1, p2)-> String.format("Flux1: %s, Flux2: %s", p1, p2))
                .subscribe(x -> log.info(x));
    }

    public void zipWith(){
        List<Persona> personas1 = new ArrayList<>();
        personas1.add(new Persona(1, "Mito", 27));
        personas1.add(new Persona(2, "Mitoc", 28));
        personas1.add(new Persona(3, "Mitoco", 29));

        List<Persona> personas2 = new ArrayList<>();
        personas2.add(new Persona(4, "Mitocod", 27));
        personas2.add(new Persona(5, "Mitoccode", 28));
        personas2.add(new Persona(6, "Mitocodedee", 29));

        List<Ventas> ventas =  new ArrayList<>();
        ventas.add(new Ventas(1, LocalDateTime.now()));

        Flux<Persona> fx1 = Flux.fromIterable(personas1);
        Flux<Persona> fx2 = Flux.fromIterable(personas2);
        Flux<Ventas> fx3 = Flux.fromIterable(ventas);

        fx1.zipWith(fx3, (p1, v) -> String.format("Flux1: %s, Flux3: %s", p1, v))
                .subscribe(x -> log.info(x));

    }

}
